import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { RegisterPhoneComponent } from './auth/register-phone/register-phone.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { LoginUsernameComponent } from './auth/login-username/login-username.component';
import { MatchesComponent } from './home/matches/matches.component';
import { PlayersComponent } from './home/players/players.component';
import { CaptainsComponent } from './home/captains/captains.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { ReportsLayoutComponent } from './layouts/reports-layout/reports-layout.component';
import { ScoreboardComponent } from './reports/scoreboard/scoreboard.component';
import { HistoryComponent } from './reports/history/history.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    LoginComponent,
    RegisterComponent,
    RegisterPhoneComponent,
    LoginUsernameComponent,
    MatchesComponent,
    PlayersComponent,
    CaptainsComponent,
    HomeLayoutComponent,
    ReportsLayoutComponent,
    ScoreboardComponent,
    HistoryComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    CarouselModule,
    MatFormFieldModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
