import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginUsernameComponent } from './auth/login-username/login-username.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterPhoneComponent } from './auth/register-phone/register-phone.component';
import { RegisterComponent } from './auth/register/register.component';
import { CaptainsComponent } from './home/captains/captains.component';
import { MatchesComponent } from './home/matches/matches.component';
import { PlayersComponent } from './home/players/players.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { HistoryComponent } from './reports/history/history.component';
import { ScoreboardComponent } from './reports/scoreboard/scoreboard.component';

const routes: Routes = [
  {
    path: '', component: HomeLayoutComponent, children: [
      { path: '', redirectTo: 'matches', pathMatch: 'full' },
      { path: 'matches', component: MatchesComponent },
      { path: 'players', component: PlayersComponent },
      { path: 'captains', component: CaptainsComponent }
    ]
  },
  {
    path: 'reports', component: HomeLayoutComponent, children: [
      { path: '', redirectTo: 'history', pathMatch: 'full' },
      { path: 'history', component: HistoryComponent },
      { path: 'scoreboard', component: ScoreboardComponent }
    ]
  },
  {
    path: 'auth', component: AuthLayoutComponent, children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent },
      { path: 'registerUsername', component: RegisterComponent },
      { path: 'loginUsername', component: LoginUsernameComponent },
      { path: 'phoneSignIn', component: RegisterPhoneComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
