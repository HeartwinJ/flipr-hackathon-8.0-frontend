import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  // tslint:disable-next-line: variable-name
  private _selectedPlayers = [];

  constructor() { }

  // tslint:disable-next-line: typedef
  public get selectedPlayers() {
    return this._selectedPlayers;
  }

  public set selectedPlayers(data: any) {
    this._selectedPlayers = data;
  }

}
