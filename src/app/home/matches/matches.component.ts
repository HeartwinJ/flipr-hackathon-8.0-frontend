import { Component, DebugEventListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss']
})
export class MatchesComponent implements OnInit {

  Matches = [
    {
      Team1 : "Delhi Dare Devils",
      Team2 : "Kolkata Knight Riders",
      Match_type : "T20"
    }
    
  ]

  constructor(private router: Router) { }
  onClick() {
    this.router.navigateByUrl('/players');
  };
  ngOnInit(): void {
  }


}
