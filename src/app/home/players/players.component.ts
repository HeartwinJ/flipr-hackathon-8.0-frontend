import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  toggleSelectPlayer(player: any) {
    let tempCreds = this.user_credits;
    player.selected? tempCreds += player.Credits : tempCreds -= player.Credits;
    if(tempCreds > 0) {
      player.selected = !player.selected;
      this.user_credits = tempCreds;
    }
  }

  players = [
    {
      Name : "Rahul Dravid",
      Credits : 200,
      selected: false
    },
    {
      Name : "Sachin Tendulkar",
      Credits : 250,
      selected: false
    },
    {
      Name : "Mahendra Sing Dhoni",
      Credits : 145,
      selected: false
    },
    {
      Name : "Virat Kohli",
      Credits : 120,
      selected: false
    },
    {
      Name : "Lebron James",
      Credits : 1,
      selected: false
    },
    {
      Name : "Vedha",
      Credits : 150,
      selected: false
    },
    {
      Name : "Melvyn",
      Credits : 100,
      selected: false
    },
    {
      Name : "Mashooq",
      Credits : 150,
      selected: false
    },
    {
      Name : "Jeeva",
      Credits : 100,
      selected: false
    },
    {
      Name : "Heartwin",
      Credits : 1500,
      selected: false
    }
  ]

  user_credits = 1000;

}


