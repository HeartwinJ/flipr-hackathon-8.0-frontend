import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-captains',
  templateUrl: './captains.component.html',
  styleUrls: ['./captains.component.scss']
})
export class CaptainsComponent implements OnInit {

  players = [
    {
      Name : "Rahul Dravid",
      Credits : 200,
      selected: false,  
      captain : false,
      vice : false
    },
    {
      Name : "Sachin Tendulkar",
      Credits : 250,
      selected: false,
      captain : false,
      vice : false    
    },
    {
      Name : "Mahendra Sing Dhoni",
      Credits : 145,
      selected: false,
      captain : false,
      vice : false    
    },
    {
      Name : "Virat Kohli",
      Credits : 120,
      selected: false,
      captain : false,
      vice : false    
    },
    {
      Name : "Lebron James",
      Credits : 1,
      selected: false,
      captain : false,
      vice : false    
    },
    {
      Name : "Vedha",
      Credits : 150,
      selected: false,
      captain : false,
      vice : false    
    },
    {
      Name : "Melvyn",
      Credits : 100,
      selected: false,
      captain : false,
      vice : false    
    },
    {
      Name : "Mashooq",
      Credits : 150,
      selected: false,
      captain : false,
      vice : false    
    },
    {
      Name : "Jeeva",
      Credits : 100,
      selected: false,
      captain : false,
      vice : false    
    },
    {
      Name : "Heartwin",
      Credits : 1500,
      selected : false,
      captain : false,
      vice : false
    }
  ]

  flag1 = true;
  flag2 = true;
  captain!: string;
  vice!: string;

  toggleSelectCaptain(player : any){
    if(this.flag1){
      player.captain = !player.captain;
      this.flag1 = !this.flag2;
      this.captain = player.Name;
    }
    if(this.flag2 && !player.captain){
      player.vice = !player.vice
      this.flag2 = !this.flag2;
      this.vice = player.Name;
    }
  }
  constructor() { }

  ngOnInit(): void {
  }

}
